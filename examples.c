#include <stdio.h>
#include <string.h>
#include <error.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>

int main(){
	some_prints();
	error_handling();

}

int error_handling(){
	FILE *fp;
	fp = fopen("/tmp/fake", "r");
	//printing error number
	printf("%d\n", errno);
	//printing error message
	char* pp[10];
	strcpy(pp, strerror(errno));
	printf("%s\n", pp);

	//Here's an alternate way to do it using perror
	int fd;
	errno = 0;
	fd = open("/tmp/fake", O_RDONLY);
	if (fd == -1){
		perror("open");
		exit(-1);
	}

	return 1;
}

int some_prints(){
	printf("Hello World!\n");
		/*simple print*/
		printf("vomi\n");

		/*printing an integer inside a printf*/
		printf("vomi %d\n", 99);

		/*printing a float inside a printf*/
		printf("vomi %f\n", 99.9);

		/*print string*/
		char vomistr[50];
		strcpy(vomistr, "vomistring");
		printf("printing string: %s\n", vomistr);

		/*looping through string and breaking when reaching end*/
		int len = strlen(vomistr);
		printf("%d\n", len);
		for(int x=0; x < len; x++){
				if (x == len-1){
					printf("%c\n",vomistr[x]);
				}
				else{
					printf("%c-",vomistr[x]);
				}
		}

		return 0;
}
